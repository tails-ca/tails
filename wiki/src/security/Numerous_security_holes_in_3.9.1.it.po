# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-10-24 04:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon, 22 Oct 2018 19:50:04 +0200\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 3.9.1\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"Several security holes that affect Tails 3.9.1 are now fixed in "
"Tails 3.10.1."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** encourage you to [[upgrade to "
"Tails 3.10.1|news/version_3.10.1]] as soon as possible."
msgstr ""

#. type: Bullet: ' - '
msgid "Tor Browser: [[!mfsa 2018-27]]"
msgstr ""

#. type: Bullet: ' - '
msgid "Thunderbird: [[!mfsa 2018-25]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
" - Linux:\n"
"   - [[!cve CVE-2018-15471]]\n"
"   - [[!cve CVE-2018-18021]]\n"
"   - [[!cve CVE-2018-13099]]\n"
"   - [[!cve CVE-2018-13100]]\n"
"   - [[!cve CVE-2018-13098]]\n"
"   - [[!cve CVE-2018-17182]]\n"
"   - [[!cve CVE-2018-7755]]\n"
"   - [[!cve CVE-2018-14633]]\n"
"   - [[!cve CVE-2018-14617]]\n"
"   - [[!cve CVE-2018-14609]]\n"
"   - [[!cve CVE-2018-14612]]\n"
"   - [[!cve CVE-2018-9363]]\n"
" - git:\n"
"   - [[!debsa2018 4311]]\n"
" - imagemagick:\n"
"   - [[!debsa2018 4316]]\n"
" - libssh:\n"
"   - [[!debsa2018 4322]]\n"
msgstr ""
